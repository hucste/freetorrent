<?php
include_once 'includes/config.php';

//Si l'utilisateur est déjà loggé, on le renvoie sur l'index
/*
if($user->is_logged_in()) {
	header('Location: ./');
}
*/

$pagetitle = 'Nous contacter';

include_once 'includes/header.php';
include_once 'includes/header-logo.php';
include_once 'includes/header-nav.php';
?>

<div class="wrapper row3">
  <div id="container">
    <!-- ### -->
    <div id="contact" class="clear">
      <div class="two_third first">


	<?php
	// Affichage : message envoyé !
	if(isset($_GET['action'])){
		echo '<div class="alert-msg rnd8 success">Votre message a bien été envoyé ! Nous y répondrons dès que possible ! <a class="close" href="#">X</a></div>';
	}
	if(isset($_GET['wrong_code'])) {
		echo '<br><div class="alert-msg rnd8 error"><span class="fa fa-warning font-medium"></span>&nbsp;Mauvais code anti-spam ! <a class="close" href="#">X</a></div>';
	}
	?>

	<?php
	//if form has been submitted process it
	if(isset($_POST['submit'])) {
		$name = html($_REQUEST["name"]);
        	$subject = html($_REQUEST["subject"]);
        	$message = nl2br(BBCode2Html($_REQUEST["message"]));
        	$from = html($_REQUEST["from"]);
        	//$verif_box = $_REQUEST["verif_box"];

 		if($name ==''){
                	$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;Veuillez entrer un pseudo !';
        	}

        	if($from ==''){
                	$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;Veuillez entrer une adresse e-mail !';
        	}

		// On vérifie l'e-mail
		if (isset($from) && !empty($from)) {
			if (!filter_var($from, FILTER_VALIDATE_EMAIL)) {
        			$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;Cette adresse e-mail n\'est pas valide !';
			}
		}

        	if($subject ==''){
                	$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;Veuillez préciser un sujet !';
        	}

        	if($message ==''){
                	$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;Votre message est vide ?!?';
        	}

		//reCaptcha
		$secret = "6LeoUy4UAAAAAAkT9167mTJxuQYQDZYW3QDs0rDh";
		$response = $_POST['g-recaptcha-response'];
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" 
        		. $secret
        		. "&response=" . $response
        		. "&remoteip=" . $remoteip ;
  		$decode = json_decode(file_get_contents($api_url), true);

		if ($decode['success'] == true) {	
			if(!isset($error)) {
				$message = "Nom: ".$name."\r\n".$message;
        			$message = "De: ".$from."\r\n".$message;
        			mail(SITEMAIL, 'Message depuis '.SITENAMELONG.' : '.$subject, $_SERVER['REMOTE_ADDR']."\r\n".$message, "From: $from");
        			header("Location: /contact.php?action=ok");
        			//setcookie('tntcon','');
			}
		}
	
		else {
    			$error[] = '<span class="fa fa-warning font-large"></span>&nbsp;ERREUR : Vous n\'avez pas validé l\'anti-spam</span>';
		}
	} // /if isset

	if(isset($error)) {
		foreach($error as $error){
        		echo '<div class="alert-msg rnd8 error">'.$error.'</div>';
        	}
	}
?>


<h2>Nous contacter :</h2>
<p>Merci d'utiliser le formulaire ci-dessous pour nous contacter :</p>
<br>

<div class="clear">

<form class="rnd5" action="" method="post">

<div class="form-input clear">

	<div class="one_half">
	<label for="name">Votre nom :
	   <?php $nom = isset($_POST['name']) ? $_POST['name'] : ''; ?>
	   <input name="name" type="text" value="<?php echo html($nom); ?>">
	<Label>
	</div>
	<div class="one_half">
	<label for="from">Votre e-mail :
	   <?php $de = isset($_POST['from']) ? $_POST['from'] : ''; ?>
	   <input name="from" type="text" value="<?php echo html($de); ?>">
	</label>
	</div>
<br><br><br>
	<label for="subject">Sujet :
	   <?php $sujet = isset($_POST['subject']) ? $_POST['subject'] : ''; ?>
	   <input name="subject" type="text" value="<?php echo html($sujet); ?>">
	</label>
<br>
	<label for="message">Message :
           <textarea rows="12" name="message"></textarea>
        </label>

<br>
	<label for="verif_box">Anti-spam : <br>
           <div class="g-recaptcha" data-sitekey="6LeoUy4UAAAAAEZu8KlzMYVtXK63LTlmSXB0gjR5"></div>
        </label>

</div>

<br><br><p>
	<div class="fl_right">
	<input name="submit" class="button small orange" type="submit" value="Envoyer le message">
	&nbsp;
	<input type="reset" value="Annuler" class="button small grey">
	</div>
</p>
</form>
<br>

</div>

	<!-- ### -->
		
	<div class="divider2"></div>
	
      </div>


<?php
include_once 'includes/sidebar.php';
include_once 'includes/footer.php';
?>
