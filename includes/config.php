<?php

//Sessions
ob_start();
session_start();

//set timezone
date_default_timezone_set('Europe/Paris');

//SQL--------------------------------------------------------------------------------
define('DBHOST','localhost');
define('DBUSER','xxxxxxxxxxxxxxx');
define('DBPASS','xxxxxxxxxxxxxxx');
define('DBNAME','xxxxxxxxxxxxxxx');

try {
	$db = new PDO("mysql:host=".DBHOST.";port=8889;dbname=".DBNAME, DBUSER, DBPASS);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
	//show error
    	echo '<p>'.$e->getMessage().'</p>';
    	exit;
}

//PARAMETRES DU SITE-----------------------------------------------------------------
define('WEBPATH','/var/www/freetorrent.fr/web/');
define('SITENAME','freetorrent');
define('SITENAMELONG','freetorrent.fr');
define('SITESLOGAN','Freetorrent : Bittorrent au service du Libre');
define('SITEDESCRIPTION','freetorrent.fr rassemble tous médias sous licence libre ou licence de libre diffusion et les propose au partage par l\'intermédiaire du protocole Bittorrent.');
define('SITEKEYWORDS','freetorrent,bittorrent,torrent,partage,échange,peer,p2p,licence,license,medias,libre,free,opensource,gnu,téléchargement,download,upload,xbt,tracker,php,mysql,linux,bsd,os,système,system,exploitation,debian,arch,fedora,ubuntu,manjaro,mint,film,movie,picture,video,mp3,musique,music,mkv,avi,mpeg,gpl,creativecommons,cc,mit,apache,cecill,artlibre');
define('SITEURL','http://www.freetorrent.fr');
define('SITEURLHTTPS','https://www.freetorrent.fr');
define('SITEMAIL','webmaster@freetorrent.fr');
define('SITEOWNORNAME','Olivier Prieur');
define('SITEOWNORADDRESS','16, rue de Chaudron 58200 Cosne-Cours-sur-Loire');
//define('SITEDISQUS','freetorrentfr');
define('ANNOUNCEPORT','55555');
define('SITEVERSION','2.0.1-4');
define('SITEDATE','08/09/17');
define('COPYDATE','2017');
define('CHARSET','UTF-8');
//Nb de torrents sur la page ... torrents.php
define('NBTORRENTS','15');
// Announce
$ANNOUNCEURL = SITEURL.':'.ANNOUNCEPORT.'/announce';
// Répertoire des images
$REP_IMAGES = '/var/www/freetorrent.fr/web/images/';

//Paramètres pour le fichier torrent (upload.php)
define('MAX_FILE_SIZE', 1048576); // Taille maxi en octets du fichier .torrent
$WIDTH_MAX = 500; // Largeur max de l'image en pixels
$HEIGHT_MAX = 500; // Hauteur max de l'image en pixels
$REP_TORRENTS = '/var/www/freetorrent.fr/web/torrents/'; // Répertoire des fichiers .torrents

//Paramètres pour l'icone de présentation du torrent (index.php, edit-post.php, ...)
$WIDTH_MAX_ICON = 150; //largeur maxi de l'icone de présentation dut orrent
$HEIGHT_MAX_ICON = 150; //Hauteur maxi de l'icone de présentation du torrent
$MAX_SIZE_ICON = 30725; // Taille max en octet de l'icone de présentation du torrent (30 Ko)
$REP_IMAGES_TORRENTS = '/var/www/freetorrent.fr/web/images/imgtorrents/';
$WEB_IMAGES_TORRENTS = 'images/imgtorrents/';

//Paramètres pour l'avatar membre (profile.php, edit-profil.php, ...)
$MAX_SIZE_AVATAR = 51200; // Taille max en octets du fichier (50 Ko)
$WIDTH_MAX_AVATAR = 200; // Largeur max de l'image en pixels
$HEIGHT_MAX_AVATAR = 200; // Hauteur max de l'image en pixels
$EXTENSIONS_VALIDES = array( 'jpg' , 'png' ); //extensions d'images valides
$REP_IMAGES_AVATARS = '/var/www/freetorrent.fr/web/images/avatars/'; // Répertoires des images avatar des membres

// Edito - Page d'accueil
$EDITO = '
<p class="justify">Depuis 2006, '.SITENAMELONG.' rassemble des projets sous licences libres et licences de libre diffusion et les propose au téléchargement par l\'intermédiaire du protocole Bittorrent.<br>
'.SITENAMELONG.' est complémentaire de certains gros projets officiels qui possèdent déjà leurs services Bittorrent et s\'adresse tout particulièrement aux projets plus modestes qui recherchent un moyen simple de partager librement leurs travaux.<br>
<br>Si vous voulez nous apporter un peu d\'aide, merci de nous contacter par l\'intermédiaire du <a href="/contact.php">formulaire de contact</a>.<br>
<br>Le téléchargement (leech) est libre et ne nécessite aucune création de compte. Néanmoins, vous devrez créer un compte membre afin d\'uploader des torrents.
</p>
';

// Deconnexion auto au bout de 10 minutes
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if (isset($_SESSION['time'])) {
        	if ($_SESSION['time'] + 600 > time()) {
                	$_SESSION['time'] = time();
             	}
		else {
			header ('Location: '.SITEURL.'/logout.php');
		}
     	}
	else {
		$_SESSION['time'] = time();
	}
}



// -----------------------------------------------------------------------------------
// CLASSES
// -----------------------------------------------------------------------------------

//load classes as needed
function __autoload($class) {
   
   $class = strtolower($class);

   //if call from within assets adjust the path
   $classpath = 'classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath;
   }  
   
   //if call from within admin adjust the path
   $classpath = '../classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath;
   }
   
   //if call from within admin adjust the path
   $classpath = '../../classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath;
   }     
    
}

require_once(WEBPATH.'classes/phpmailer/mail.php');

$user = new User($db); 

// On inclut le fichier de fonctions
// et les fichiers d'encodage et de décodage des torrents 
require_once('functions.php');
require_once('BDecode.php');
require_once('BEncode.php');

?>
