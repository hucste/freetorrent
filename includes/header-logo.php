<!-- ################################### header-logo.php ############################################ -->
<body id="top" class="">
<div class="wrapper row1">
	
<div class="bgded overlay" style="background-image:url('layout/styles/images/back.jpg');">

  <header id="header" class="full_width clear">
    <div id="hgroup" class="logo">
      <h1><a href="./"><?php echo SITENAMELONG; ?></a></h1>
      <h2><?php echo SITESLOGAN; ?></h2>
    </div>
    <div id="header-contact">
      <ul class="list none">
	    <?php if($pagetitle == 'Bienvenue sur '.SITENAMELONG.' !') {echo '<li class="active">';} else {echo '<li>';} ?><span class="fa fa-home"></span> <a href="./">Accueil</a></li>
	    <?php
		/*
		if($pagetitle == 'Connexion membres') {
			echo '<li class="active">';
		}
		else {
			echo '<li>';
		}

		if(isset($_SESSION['username'])) {
			echo '<span class="fa fa-sign-out"></span> <a href="/logout.php">Déconnexion</a></li>';
		}
		else {
			echo '<span class="fa fa-sign-in"></span> <a href="/login.php">Connexion</a></li>';
		}
		*/
	   ?>
           <?php if($pagetitle == 'Nous contacter') {echo '<li class="active">';} else {echo '<li>';} ?><span class=" fa fa-envelope-o"></span> <a href="/contact.php">Nous contacter</a></li>
           <?php if($pagetitle == 'A propos') {echo '<li class="active">';} else {echo '<li>';} ?><span class="fa fa-info"></span> <a href="/apropos.php">A propos</a></li>
	   <li><span class="fa fa-lock"></span>&nbsp;<span class="font-tiny"><a href="<?php echo SITEURLHTTPS; ?>">Version HTTPS</a></span></li>
      </ul>
      <div class="fl_right">
         <a class="font-large" href="https://twitter.com"><span class="fa fa-twitter"></span></a>&nbsp;&nbsp;<a class="font-large" href="https://plus.google.com"><span class="fa fa-google-plus"></span></a>&nbsp;&nbsp;<a class="font-large" href="https://www.facebook.com/"><span class="fa fa-facebook"></span></a>&nbsp;&nbsp;<a class="font-large" href="/rss.php"><span class="fa fa-feed"></span></a>
      </div>
    </div> <!-- /header-contact -->
  </header>

</div> <!-- /class bgded overlay -->
