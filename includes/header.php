<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="<?php echo CHARSET; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-dns-prefetch-control" content="off">
	<base href="/">
	<title><?php echo $pagetitle; ?></title>
	<meta name="description" content="<?php echo SITEDESCRIPTION; ?>">
	<meta name="keywords" content="<?php echo SITEKEYWORDS; ?>">
	<meta name="author" content="<?php echo SITEOWNORNAME; ?>">

	<link rel="shortcut icon" href="/images/favicon.ico">
	<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
	<link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">

	<!--[if lt IE 9]>
	<link href="layout/styles/ie/ie8.css" rel="stylesheet" type="text/css" media="all">
	<script src="layout/scripts/ie/css3-mediaqueries.min.js"></script>
	<script src="layout/scripts/ie/html5shiv.min.js"></script>
	<![endif]-->

	<!-- IE9 Placeholder Support -->
	<script src="layout/scripts/jquery.placeholder.min.js"></script>

	<!-- jQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<!-- reCaptcha -->
	<script src="https://www.google.com/recaptcha/api.js?hl=fr"></script>

	<!-- Wysibb -->
	<script src="/layout/scripts/wysibb/jquery.wysibb.min.js"></script>
	<link rel="stylesheet" href="/layout/scripts/wysibb/theme/default/wbbtheme.css"/>
	<script src="/layout/scripts/wysibb/lang/fr.js"></script>
	<script>
		$(function() {
		$("#editor").wysibb();
		})
	</script>

	<!-- Password -->	
   	<script type="text/javascript" language="javascript">
	jQuery(document).ready(function() {
		$('#username').keyup(function(){$('#result').html(passwordStrength($('#password').val(),$('#username').val()))})
		$('#password').keyup(function(){$('#result').html(passwordStrength($('#password').val(),$('#username').val()))})
	})
	function showMore()
	{
		$('#more').slideDown()
	}
   	</script>

    	<!-- Suppression d'un post (torrent) par son auteur -->
    	<script language="JavaScript" type="text/javascript">
        function deltorr(id, title) {
                if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
                        window.location.href = '../viewpost.php?deltorr=' + id;
                }
        }
    	</script>

    	<!-- Suppression d'un post (torrent) par l'Admin -->
    	<script language="JavaScript" type="text/javascript">
	function delpost(id, title) {
		if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
			window.location.href = '/admin/index.php?delpost=' + id;
		}
	}
    	</script>

    	<!-- Suppression d'une catégorie par l'Admin -->
    	<script language="JavaScript" type="text/javascript">
	function delcat(id, title) {
		if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
			window.location.href = '/admin/categories.php?delcat=' + id;
		}
	}
    	</script>

    	<!-- Suppression d'une licence par l'Admin -->
    	<script language="JavaScript" type="text/javascript">
	function dellicence(id, title) {
		if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
			window.location.href = '/admin/licences.php?dellicence=' + id;
		}
	}
    	</script>

    	<!-- Suppression d'un membre par l'Admin -->
    	<script language="JavaScript" type="text/javascript">
	function deluser(id, title) {
		if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
			window.location.href = '/admin/users.php?deluser=' + id + '&delname=' + title;
		}
	}
    	</script>

    	<!-- Suppression de l'avatar du membre -->
    	<script language="JavaScript" type="text/javascript">
        function delavatar(id, title) {
                if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
                        window.location.href = 'edit-profil.php?delavatar=' + id + '&delname=' + title;
                }
        }
    	</script>

    	<!-- Suppression de l'image du torrent -->
    	<script language="JavaScript" type="text/javascript">
	function delimage(id, title) {
		if (confirm("Etes-vous certain de vouloir supprimer '" + title + "'")) {
			window.location.href = '/admin/edit-post.php?delimage=' + id;
		}
	}
    	</script>

</head>

<?php
$url = $_SERVER['REQUEST_URI'];
define("_BBC_PAGE_NAME", "$url");
define("_BBCLONE_DIR", "/var/www/freetorrent.fr/web/bbclone/");
define("COUNTER", _BBCLONE_DIR."mark_page.php");
if (is_readable(COUNTER)) include_once(COUNTER);
?>
